#UpdateFiles

### 此项目实现软件的自动下载并运行，为软件更新插件，辅助实现软件的自动更新功能。


 **方式一：** 

用法很简单,如下载地址中easyIcon软件：

UpdateFiles.exe "https://git.oschina.net/scimence/easyIcon/raw/v2016_06_09/files/easyIcon.exe" "esayIcon.exe"

![输入图片说明](http://git.oschina.net/uploads/images/2016/0526/203748_266ae3bc_334438.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0526/203816_ab855d5e_334438.jpeg "在这里输入图片标题")

我们可在需要自动更新的软件中，调用以下语句进行新版本的下载更新：

![输入图片说明](http://git.oschina.net/uploads/images/2016/0526/204856_8a080d60_334438.jpeg "在这里输入图片标题")

 **方式二：** 

在项目内部添加自动更新逻辑

![输入图片说明](http://git.oschina.net/uploads/images/2016/0609/113738_6a2aedaf_334438.jpeg "在这里输入图片标题")

[自动更新逻辑，相关类文件](https://git.oschina.net/scimence/UpdateFiles/tree/master/files)


 **下载：** 
[UpdateFiles.exe](https://git.oschina.net/scimence/UpdateFiles/raw/master/files/UpdateFiles.exe)